#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p "$SCRIPT_DIR/build/"
cd "$SCRIPT_DIR/build/"

wget -c 'http://download.redis.io/redis-stable.tar.gz'
tar xzf "redis-stable.tar.gz"
cd redis-stable/
make -j8
make PREFIX="$SCRIPT_DIR/../" install

cd "$SCRIPT_DIR"
rm -rf "$SCRIPT_DIR/build/"
