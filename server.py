from flask import Flask
from flask import Response

import os
import pathlib
import random
import time

# Demo from http://flask.pocoo.org/docs/0.12/patterns/streaming/

app = Flask(__name__)

@app.route('/')
def index():
    with open('index.html') as f:
        content = f.read()
    return content


@app.route('/video-yield.mp4')
def data_video_yield():
    def generate_data():
        script_path = pathlib.Path(os.path.dirname(__file__))
        file_Path = script_path / '人民的名义 01.mp4'
        file_size = file_Path.stat().st_size
        chunk_num = 1000
        with file_Path.open(mode='rb') as f:
            for ch in range(chunk_num):
                content  = f.read(file_size // chunk_num)
                yield content
                sleep_time = random.uniform(0.002, 0.005)
                time.sleep(sleep_time)
            content = f.read()
            yield content
    return Response(generate_data(), mimetype='video/mp4')


@app.route('/video-read.mp4')
def data_video_read():
    script_path = pathlib.Path(os.path.dirname(__file__))
    file_Path = script_path / '人民的名义 01.mp4'
    with open(str(file_Path.absolute()), mode='rb') as f:
        content = f.read()
    return Response(content, mimetype='video/mp4')




if __name__ == "__main__":
    app.run()
