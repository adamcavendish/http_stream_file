import flask

# http://python-rq.org/
# RQ is an async task queue project for running tasks in background
# You can put your downloading to background with rq
import rq

# sh is used to execute shell commands
import sh

import requests

import hashlib
import io
import json
import os
import pathlib
import random
import time

script_Path = pathlib.Path(os.path.dirname(__file__))

app = flask.Flask(__name__)

@app.route('/')
def index():
    with open('html/index.html') as f:
        content = f.read()
    return content


def sha256_from_string(s):
    m = hashlib.sha256()
    m.update(s.encode())
    return m.hexdigest()


def video_info_by_url(video_url):
    # https://amoffat.github.io/sh/sections/faq.html#how-do-i-execute-a-program-with-a-dash-in-its-name
    with io.StringIO() as buf:
        sh.you_get('--json', video_url, _out=buf)
        return json.loads(buf.getvalue())


def download_one_segment(segment_url, filePath):
    header = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
    pass


def download_segments(video_url, segments, container):
    file_id = sha256_from_string(video_url)
    filePath = script_Path / "videos" / file_id
    for segment in segments:
        download_one_segment(segment, filePath)


@app.route('/download-video-post', methods=['POST'])
def download_video_post():
    post = flask.request.get_json()
    video_url = post.get('video-url')

    video_info = video_info_by_url(video_url)

    # Download the smallest video stream
    vid = video_info['autdiolang']
    streams = video_info['streams']
    streams_size = [(k, v['size']) for k, v in streams.items()]
    streams_size.sort(key=lambda item: item[1])
    download_stream_name = streams_size[0][0]
    download_stream_info = streams[download_stream_name]

    print(video_info)
    return

    def generate_data():

        file_Path = script_path / 'videos' / '人民的名义 01.mp4'
        file_size = file_Path.stat().st_size
        chunk_num = 1000
        with file_Path.open(mode='rb') as f:
            for ch in range(chunk_num):
                content  = f.read(file_size // chunk_num)
                yield content
                sleep_time = random.uniform(0.002, 0.005)
                time.sleep(sleep_time)
            content = f.read()
            yield content
    return flask.Response(generate_data(), mimetype='video/mp4')

@app.route('/download-video-get', methods=['GET'])
def download_video_get():
    pass


if __name__ == "__main__":
    app.run()
